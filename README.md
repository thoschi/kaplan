KAplan ist ein Klassenarbeitsplaner auf Basis von Flask.

Dieses Werkzeug ersetzt bei uns das Papier-Heft, in das Kolleginnen und Kollegen Klassenarbeiten eintragen können.

Es greift auf den LDAP eines linuxmuster.net-Servers zurück, dürfte aber auch für andere OpenLDAP/Samba/AD-Server einfach anzupassen sein.

Die Einträge/Gruppen/etc. sind in einer MySQL/MariaDB-Datenbank gespeichert, der Rest besteht aus einer Python-Datei, zwei HTML-Templates und einigen Bibliotheken.

![Screenshot](static/img/screenshot.png "Screenshot")

Einige weitere Features:

- Anzeige in Kalenderform (Danke an fullcalendar.io)
- Ausblendbare Seitenleiste zum Filtern von Jahrgängen/Klassen über Auswahlfelder, Buttons und ein Freitext-Feld
- bequeme Tastatursteuerung (für weniger Klicks)
- konfigurierbare maximale Anzahl von Arbeiten
- Einfügen von blockierenden und nichtblockierenden und über CSS konfigurierbaren Farbmarkierungen im Kalender über die Datenbank
- Datenbank-Einrichtungs-Skript zum einfachen Anlegen der Datenbanken, Farbcodes oder zum Zurücksetzen beim Testen
- Export in ICS-Dateien

DIESES PROJEKT IST IM BETA-STADIUM. TESTS UND RÜCKMELDUNGEN SEHR WILLKOMMEN.

*****

KAplan is a exam-planner based on Flask.

This tool is a replacement for the paper-notebook we had at school, that we used to plan exams for our classes.

It authenticates and uses the LDAP of a linuxmuster.net-server but should be easily adoptable to any OpenLDAP/Samba/AD-server.

All entries/groups/... are stored in a MySQL/MariaDB-database, besides that it needs only a python-file (using flask), two HTML-templates and some libraries.

![Screenshot](static/img/screenshot.png "Screenshot")

Some features:

- convenient calendar-view (thanks to fullcalendar.io)
- hideable sidebar for filtering years/groups using selects, buttons and an input
- filtering can be done by keyboard (efficiently saving some clicks)
- max. number of exams per week is configurable
- day-entries in the database are shown in the calendar (CSS-styled) in a blocking (no more entries possible for that day) or non-blocking style
- simple script for setting up/resetting the database or for changing the color scheme available
- export to ics files

THIS PROJECT IS STILL BETA. TESTS AND FEEDBACK WELCOME.
