# flask-related
from flask import Flask, render_template, request, jsonify, session, redirect, url_for, render_template_string, send_file, flash
from flask_ldap3_login import LDAP3LoginManager
from flask_ldap3_login.forms import LDAPLoginForm
from flask_login import LoginManager, login_user, logout_user, UserMixin, current_user

# database
from flaskext.mysql import MySQL

# for reading variables from .env
import os
from dotenv import load_dotenv, find_dotenv

# needed for ics export
from datetime import datetime, timedelta
import io 
from icalendar import Calendar, Event, vCalAddress, vText
from pathlib import Path
import os
import pytz
import json


# load the environment variables
load_dotenv(find_dotenv())

app = Flask(__name__)

app.config['DEBUG'] = os.environ.get("DEBUG")
app.config['SECRET_KEY'] = os.environ.get("SECRET_KEY")

app.config['MAX_EXAMS_PER_WEEK'] = int(os.environ.get("MAX_EXAMS_PER_WEEK", default=3))

app.config['EDITOR_GROUPS'] = json.loads(os.environ.get("EDITOR_GROUPS", default=["teachers"]))
app.config['MODERATORS'] = json.loads(os.environ.get("MODERATORS", default=["ki", "sue"]))

app.config['SUBJECTS'] = json.loads(os.environ.get('SUBJECTS', default=[]))

# MySQL connection
app.config['MYSQL_DATABASE_HOST'] = os.environ.get("DBHOST")
app.config['MYSQL_DATABASE_USER'] = os.environ.get("DBUSER")
app.config['MYSQL_DATABASE_PASSWORD'] = os.environ.get("DBPASS")
app.config['MYSQL_DATABASE_DB'] = os.environ.get("DBNAME")


# needed for message flashing callbacks
app.config['SESSION_COOKIE_SAMESITE'] = 'None'
app.config['SESSION_COOKIE_SECURE'] = 'True'

mysql = MySQL()
mysql.init_app(app)


def contrasting_text_color(hex_str):
   '''helper function to find the best readable text colour'''
   (r, g, b) = (hex_str[1:3], hex_str[3:5], hex_str[5:7])
   return '#000000' if 1 - (int(r, 16) * 0.299 + int(g, 16) * 0.587 + int(b, 16) * 0.114) / 255 < 0.5 else '#ffffff'

def get_list(table, cfilter = None):   
   ''' helper function for db access - creates a list of dicts from database tables'''
   cursor = mysql.get_db().cursor()
   # read subjects and groups from db
   # empty filter
   if cfilter == []:
      return []

   if table == 'groups' or table == 'cohorts':
      sqlcmd = "SELECT * FROM groups"
      fields = ["id", "name", "cohort", "color"]
   elif table == 'categories':
      sqlcmd = "SELECT * FROM categories"
      fields = ["id", "text", "color", "blocks"]
   elif table == 'labels':
      sqlcmd = "SELECT labels.id, labels.date, labels.text, categories.text, categories.color, categories.blocks FROM labels LEFT JOIN categories ON labels.category_id = categories.id"
      fields = ["id", "date", "text", "ctext", "ccolor", "cblocks"]
   elif table == 'events':
      sqlcmd = "SELECT events.id, groups.name, events.date, events.text, events.user, events.cdate, events.subject, events.disabled, events.moderator FROM events LEFT JOIN groups ON events.group_id = groups.id"
      fields = ["id", "group", "date", "text", "user", "cdate", "subject", "disabled", "moderator"]
   # add a filter when set
   if cfilter and (table == 'groups' or table == 'events'):
      sqlcmd += " WHERE groups.id in "+str(tuple(cfilter)) if len(cfilter) > 1 else " WHERE groups.id='" + cfilter[0] + "'"
   # add ordering
   if table == 'groups' or table == 'cohorts':
      sqlcmd += " ORDER BY id"

   #input("sqlcmd:" + sqlcmd) # DEBUG: show sql command
   
   cursor.execute(sqlcmd)
   result = cursor.fetchall()
   cursor.close()
   # use dict comprehension to create dicts from cursor results using the fields list above
   result_list = []
   for r in result:
      r_dict = {fields[i]:(r[i] if r[i] else "-") for i in range(0,len(fields))}
      result_list.append(r_dict)
   
   if table == 'cohorts':
      # cohorts (simple list, create from groups dict, removing duplicates)
      result = list(dict.fromkeys([r["cohort"] for r in result_list if r["cohort"]]))
   else:
      result = result_list
   
   #print("result", result) # DEBUG: show results   
   
   return result

# Setup LDAP Configuration Variables. Change these to your own settings.
# All configuration directives can be found in the documentation.
app.config['LDAP_HOST'] = os.environ.get("LDAP_HOST")
app.config['LDAP_BASE_DN'] = os.environ.get("LDAP_BASE_DN")
app.config['LDAP_USER_DN'] = os.environ.get("LDAP_USER_DN")
app.config['LDAP_GROUP_DN'] = os.environ.get("LDAP_GROUP_DN")
#app.config['LDAP_GROUP_OBJECT_FILTER'] = os.environ.get("LDAP_GROUP_OBJECT_FILTER")
app.config['LDAP_USER_RDN_ATTR'] = os.environ.get("LDAP_USER_RDN_ATTR")
app.config['LDAP_USER_LOGIN_ATTR'] = os.environ.get("LDAP_USER_LOGIN_ATTR")
app.config['LDAP_BIND_USER_DN'] = os.environ.get("LDAP_BIND_USER_DN")
app.config['LDAP_BIND_USER_PASSWORD'] = os.environ.get("LDAP_BIND_USER_PASSWORD")
app.config['LDAP_USER_SEARCH_SCOPE'] = os.environ.get("LDAP_USER_SEARCH_SCOPE")


login_manager = LoginManager(app)             # Setup a Flask-Login Manager
ldap_manager = LDAP3LoginManager(app)         # Setup a LDAP3 Login Manager.

# dictionary storing authenticated users
users = {}

# user object model complying with the flask-login UserMixin mixin
class User(UserMixin):
   def __init__(self, dn, username, data):
      self.dn = dn
      self.username = username
      self.data = data

   def __repr__(self):
      return self.dn

   def get_id(self):
      return self.dn

# user loader for Flask-Login (returns user from dict or None
@login_manager.user_loader
def load_user(id):
   if id in users:
      return users[id]
   return None

# user saver for Flask-Ldap3-Login (called whenever a LDAPLoginForm() successfully validates)
@ldap_manager.save_user
def save_user(dn, username, data, memberships):
   user = User(dn, username, data)
   users[dn] = user
   return user

def get_userdict():
   uid = current_user.data['sAMAccountName']
   #determine group from homeDirectory and set the editor-flag
   #gid = 'teacher' if current_user.data['gidNumber'] == 10000 else current_user.data['homeDirectory'].split('/')[3]
   # determine group from linuxmuster AD now (sophomorixAdminClass is our friend now)
   gid = current_user.data['sophomorixAdminClass']
   # check for editor group and moderator list
   editor = True if gid in app.config['EDITOR_GROUPS'] else False
   moderator = True if uid in app.config['MODERATORS'] else False
   # create a user dict
   user = { 'uid': uid,
            'gid': gid,
            'editor': editor,
            'moderator': moderator }
   return user

# Home page
@app.route("/")
def home():
   # Redirect users who are not logged in.
   if not current_user or current_user.is_anonymous:
       return redirect(url_for('login'))

   user = get_userdict()
   #input("current_user.data:", current_user.data) # DEBUG the current_user-object contains the ldap data
   
   group_list = get_list('groups')
   if not user['editor']:
      # non-editors see their own group only (by a pre-filtered get_list)
      group_list = [g for g in group_list if g['name'] == user['gid']]
   
   # read list of categories (for labels) and labels itself to mark specific days to certain classes (block/holiday/...) from db
   categories = get_list("categories")
   labels = get_list("labels")
   
   subjects = app.config['SUBJECTS']

   # TODO: admin user (with possibility to add groups/...?)
   # render the index.html template
   return render_template("index.html", \
                          cohort_list=get_list('cohorts'), \
                          group_list=group_list, \
                          subjects=subjects,\
                          user=user,\
                          labels=labels,\
                          categories=categories)

@app.route('/login', methods=['GET', 'POST'])
def login():
   '''renders the login template and logs the user in'''
   # LDAPLoginForm including validator to check if the user exists in LDAP.
   form = LDAPLoginForm()

   if form.validate_on_submit():
      # login successfull - saved user object accessible via form.user.
      login_user(form.user)  # Tell flask-login to log them in.
      flash('Erfolgreich angemeldet')
      return redirect('/')  # Send them home

   return render_template('login.html', form=form)

@app.route('/logout', methods=['GET', 'POST'])
def logout():
   # clear the session variable and log out the user
   [session.pop(key) for key in list(session.keys())]
   logout_user()
   return redirect('/')  # Send them home

# Add event
@app.route("/add-event", methods=["POST"])
def add_event():
   # get data from form and else
   addgroupsel = request.form.getlist("addgroupsel[]")

   adddate = request.form["adddate"]
   addtext = request.form["addtext"]
   user = current_user.data['sAMAccountName']
   date = datetime.now().strftime("%d.%m.%Y, %H:%M")
   addsubject = request.form["addsubject"]
   disabled = "0"
   moderator = 1 if request.form['eventModerated'] == "true" else 0

   # check if already another test in this group on that day (disabled events not counted)
   # ~ cursor = mysql.get_db().cursor()
   # ~ result = cursor.execute("SELECT id FROM events WHERE disabled='0' AND group_id ='" + addgroupsel + "' AND date='" + adddate + "'")
   # ~ cursor.close()
   # ~ if result != 0:
      # ~ return jsonify(message='Fehler: an diesem Tag wird schon eine Arbeit in dieser Klasse geschrieben'), 500

   # check if number of tests written in this week exceeds MAX_EXAMS_PER_WEEK (disabled events not counted)
   cursor = mysql.get_db().cursor()

   for g in addgroupsel:
   
      result = cursor.execute("SELECT id FROM events WHERE disabled='0' AND group_id='" + g + "' AND YEARWEEK(date)=YEARWEEK('"+ adddate + "')")
      
      if result >= app.config['MAX_EXAMS_PER_WEEK']:
         return jsonify(message='Fehler: maximal zulässige Anzahl an Arbeiten (' + str(app.config['MAX_EXAMS_PER_WEEK']) + ') erreicht'), 500

      # Insert event into MySQL database
      sql = "INSERT INTO events (group_id, date, text, user, cdate, subject, disabled, moderator) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
      val = (g, adddate, addtext, user, date, addsubject, disabled, moderator)
      
      cursor.execute(sql, val)
   
   try:
      mysql.get_db().commit()
      cursor.close()
   except:
      flash("Eintrag konnte nicht hinzugefügt werden")
      return jsonify(message='Fehler: Eintrag konnte nicht hinzugefügt werden'), 500
   flash("Eintrag hinzugefügt")
   return "Eintrag hinzugefügt!"

# Delete event
@app.route("/delete-event", methods=["POST"])
def delete_event():
   # Get event id
   event_id = request.form["event_id"].split(',')
   event_creator = request.form["event_creator"]
   
   # get user object
   user = get_userdict();

   # own entry or moderator -> delete / others entry -> disable
   if event_creator == user['uid'] or user['moderator']:
      cursor = mysql.get_db().cursor()
      # Delete event from MySQL database
      print("e_id", event_id)
      for e in event_id:
         sql = "DELETE FROM events WHERE id = %s"
         val = (e)
         print("sql", sql, val)
         cursor.execute(sql, val)
      mysql.get_db().commit()
      cursor.close()
      flash("Eintrag gelöscht")
      return "Löschen erfolgreich!"
   else:
      disabled = "1;" + user['uid'] + ";" + datetime.now().strftime("%d.%m.%Y, %H:%M")
      # change disabled-state
      sql = "UPDATE events SET disabled=%s WHERE id = %s"
      val = (disabled, event_id[0])
      cursor = mysql.get_db().cursor()
      cursor.execute(sql, val)
      mysql.get_db().commit()
      cursor.close()
      flash ("Eintrag deaktiviert!")
      return "Eintrag deaktiviert!"

# add label
@app.route("/add-label", methods=["POST"])
def add_label():
   # get data from form and else
   label_date     = request.form['label_date'].split(',')
   label_category = request.form['label_category']
   label_text     = request.form['label_text']
   
   for d in label_date:
      try:
         # delete old entries for each date
         sql = "DELETE FROM labels WHERE date = '" + d + "'"
         cursor = mysql.get_db().cursor()
         cursor.execute(sql)
         
         # Insert event into MySQL database
         sql = "INSERT INTO labels (date, category_id, text) VALUES (%s, %s, %s)"
         val = (d, label_category, label_text)
         cursor = mysql.get_db().cursor()
         cursor.execute(sql, val)
         mysql.get_db().commit()
         cursor.close()
      except:
         flash("Markierung konnte nicht hinzugefügt werden")
         return jsonify('Markierung konnte nicht hinzugefügt werden'), 500
   flash("Markierung hinzugefügt")
   return "Markierung hinzugefügt!"

# delete label
@app.route("/delete-label", methods=["POST"])
def delete_label():
   # get data from form and else
   label_date     = request.form['label_date'].split(',')
   
   for d in label_date:
      try:
         # delete old entries for each date
         sql = "DELETE FROM labels WHERE date = '" + d + "'"
         cursor = mysql.get_db().cursor()
         cursor.execute(sql)
         
         mysql.get_db().commit()
         cursor.close()
      except:
         flash("Markierung konnte nicht entfernt werden")
         return jsonify(message='Fehler: Label konnte nicht entfernt werden'), 500
   flash("Markierung entfernt")
   return "Label entfernt!"



# Get events
@app.route("/get-events", methods=['GET', 'POST'])
def get_events():

   # we need to know the role of the user for filtering deactivated evens
   user = get_userdict()
   
   # the selcted group ids
   gfilter = request.form.getlist('sc')[0].split(',')
   
   # get the filtered group list (for colors)
   groups = get_list('groups', gfilter)
   
   # get the filtered events list
   events = get_list('events',gfilter)

   # translate event-dict to filtered event list for fullcalendar
   events_list = []
   for e in events:
      # no deactivated events for students
      if user['editor'] or e['disabled'] == '0':
         # get the color from corresponding entry in groups list
         backgroundcolor = next((g['color'] for g in groups if g['name'] == e['group']), None)
         event_dict = {
            "id": e['id'],
            "title": e['group']+" - "+e['subject'],
            "allDay": 'true',
            "start": e['date'],
            "description": e['text'] + "<br/>erzeugt von " + e['user'] + " am "+e['cdate'],
            "creator": e['user'],
            "cdate": e['cdate'],
            "group": e['group'],
            "backgroundColor": backgroundcolor,
            "textColor": contrasting_text_color(backgroundcolor),
            "disabled": "false",
            "moderator": "false"
         }
         # disabled events have another line in description and get another style
         if e['disabled'] != '0':
            # change disabled-flag and add deactivation-infos to description
            duser, ddate = [e['disabled'].split(';')[i] for i in (1, 2)]
            event_dict['description'] += "<br/> deaktiviert durch " + duser + " am " + ddate
            event_dict['disabled'] = "true"

         # moderator events have another line in description and get another style
         if e['moderator'] != '0':            
            event_dict['moderator'] = "true"

         events_list.append(event_dict)
   #input(events_list) # DEBUG show the fetched events
   return jsonify(events_list)
   
@app.route('/export', methods=['GET'])
def export():
   
   # the selcted group ids
   gfilter = request.args.get('groups').split(',')
   
   # get the filtered events list
   events = get_list('events',gfilter)
   
   # get necessary form data (ie group filters)
   vfile = io.BytesIO()

   # init the calendar and add some required properties
   cal = Calendar()
   cal.add('prodid', '-//KAplan//https://gitlab.com/thoschi/kaplan//')
   cal.add('version', '2.0')
   # fill calendar from events
   for e in events: 
      event = Event()
      event.add('name', e['disabled']) # disabled -> name
      event.add('summary', e['group'] + ' - ' + e['subject'])
      event.add('description', e['text'])
      event.add('dtstart', datetime.strptime(e['date'], '%Y-%m-%d').replace(tzinfo=pytz.UTC))
      
      # Add the event to the calendar
      cal.add_component(event)

   # write ics file
   vfile.write(cal.to_ical())
   vfile.seek(0)
   #vfile.close() # when to do this (if to do it)
   #return ics file for download
   return send_file(vfile, as_attachment=True, download_name="export.ics", mimetype="text/calendar")

@app.route('/flash_message', methods=['POST'])
def flash_message():
   message = request.form.get('message')
   if message and message != "":
      flash(message)
   return jsonify("Message sent")


if __name__ == "__main__":
   app.run(host=os.environ.get('FLASK_HOST'), port=os.environ.get('FLASK_PORT'))
