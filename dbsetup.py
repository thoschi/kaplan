# flask
from flask import Flask, render_template, request, jsonify, session, redirect, url_for, render_template_string, send_file

# database
from flaskext.mysql import MySQL

# for reading variables from .env
import os
from dotenv import load_dotenv, find_dotenv

# load the environment variables
load_dotenv(find_dotenv())

app = Flask(__name__)
app.config['DEBUG'] = os.environ.get("DEBUG")
app.config['SECRET_KEY'] = os.environ.get("SECRET_KEY")

# MySQL connection
app.config['MYSQL_DATABASE_HOST'] = os.environ.get("DBHOST")
app.config['MYSQL_DATABASE_USER'] = os.environ.get("DBUSER")
app.config['MYSQL_DATABASE_PASSWORD'] = os.environ.get("DBPASS")
app.config['MYSQL_DATABASE_DB'] = os.environ.get("DBNAME")

mysql = MySQL()
mysql.init_app(app)

# actual classes <-- change this to your needs
classes = [
   '5a', '5b', '5c', '5d', '5e', '5m',
   '6a', '6b', '6c', '6m',
   '7a', '7b', '7c', '7m',
   '8a', '8b', '8c', '8d', '8m', '8s',
   '9a', '9b', '9c', '9d', '9e',
   '10a', '10b', '10m', '10p', 
   '11a', '11b', '11c', '11d', '11e', 
   'Q1', 'Q2'
]

# categories <-- change this to your needs
categories = [
  ['Hauptfach', '#F5EB9F80', 0],
  ['Nebenfach', '#F5F1D080', 0],
  ['Ferien', '#CFF5BC80', 1],
  ['Feiertag', '#D9F5CB80', 1]
]

# colorlist (try to keep this unchanged)
cohorts = {
   'Jg. 5': ['5a', '5b', '5c', '5d', '5e', '5m'],
   'Jg. 6': ['6a', '6b', '6c', '6d', '6e', '6f', '6m'],
   'Jg. 7': ['7a', '7b', '7c', '7d', '7e', '7f', '7m'],
   'Jg. 8': ['8a', '8b', '8c', '8d', '8s', '8m'],
   'Jg. 9': ['9a', '9b', '9c', '9d', '9e', '9m'],
   'Jg. 10': ['10a', '10b', '10m', '10p'],
   'Jg. 11': ['11a', '11b', '11c', '11d', '11e'],
   'Jg. Q1/Q2': ['Q1', 'Q2']
}

# colorgroups contain different lists of colours - each group will take one by one color
# from one of the sublists - check that there are more colors than group members!

colorgroups = [
               ['#1AE851', '#23DB70', '#5FE868', '#63F285', '#65DB94', '#63F2B4', '#4DEBC2'],
               ['#EBE305', '#F5E732', '#DBC927', '#F2D61F', '#E8C61E', '#F5F331', '#F5ED6F'],
               ['#EB2209', '#F54038', '#DB2C32', '#DB425B', '#EB3B4F', '#F23D56', '#EB4F3B'],
               ['#B97AEB', '#BF38F5', '#C52CDB', '#F51DED', '#EB23AB', '#DE76EB', '#DA1DF5'],
               ['#4029DB', '#3A1BF5', '#4E36F5', '#6A2CF5', '#6A39DB', '#8578EB', '#8573EB'],
               ['#F56222', '#F5733D', '#DB6430', '#EB9D7A', '#EB9E7F', '#F55736', '#F56B51'],
               ['#40C3F5', '#5ECCEB', '#1B9FF5', '#29A1DB', '#76D8F5', '#51BDF5', '#63B4EB'],
               ['#9F24EB', '#9114F5']
              ]

with app.app_context():
   db = mysql.get_db() # database available only in app-context
   cursor = db.cursor()
   while True:
      choice = input("Was tun?\n\
(1) Datenbank einrichten/zurücksetzen\n\
(2) Gruppen/Farben zurücksetzen\n\
(3) Termine zurücksetzen\n\
(4) Farbthema überschreiben\n\
(S!) Schreiben und beenden\n\
(x) Beenden\n\
1, 2, 3, 4, 5, S\, x? ")
      if choice == "1":
         if input("Datenbank wird neu erstellt. Sicher (J/n)? ") == "J":
            # setup database tables if necessary
            sql = "DROP TABLE IF EXISTS `events`;"
            cursor.execute(sql)
            print("events-Tabelle gelöscht")
            sql = "CREATE TABLE `events` (\
                    `id` int(11) NOT NULL AUTO_INCREMENT,\
                    `group_id` int(11) DEFAULT NULL,\
                    `date` varchar(100) DEFAULT NULL,\
                    `text` varchar(100) DEFAULT NULL,\
                    `user` varchar(100) DEFAULT NULL,\
                    `cdate` varchar(100) DEFAULT NULL,\
                    `subject_id` int(11) DEFAULT NULL,\
                    `disabled` varchar(100) DEFAULT '0',\
                    PRIMARY KEY (`id`)\
                  ) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;"
            cursor.execute(sql)
            print("events-Tabelle angelegt")
            
            sql = "DROP TABLE IF EXISTS `groups`;"
            cursor.execute(sql)
            print("groups-Tabelle gelöscht")
            
            sql = "CREATE TABLE `groups` (\
                    `id` int(11) NOT NULL AUTO_INCREMENT,\
                    `name` varchar(100) DEFAULT NULL,\
                    `cohort` varchar(100) DEFAULT NULL,\
                    `color` varchar(100) DEFAULT NULL,\
                    PRIMARY KEY (`id`)\
                  ) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;"
            cursor.execute(sql)
            print("groups-Tabelle angelegt")

            sql = "DROP TABLE IF EXISTS `days`;"
            cursor.execute(sql)
            print("days-Tabelle gelöscht")

            sql = "CREATE TABLE `days` (\
                    `id` int(11) NOT NULL AUTO_INCREMENT,\
                    `date` varchar(100) DEFAULT NULL,\
                    `type` varchar(100) DEFAULT NULL,\
                    `desc` varchar(100) DEFAULT NULL,\
                    `blocking` varchar(100) NOT NULL DEFAULT '0',\
                    PRIMARY KEY (`id`)\
                  ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;"
            cursor.execute(sql)
            print("days-Tabelle angelegt")

            #db.commit()
            db.rollback()
            print("  - Tabelle classes gelöscht")
            print("---> erledigt")
         else:
            print("X Abbruch")
      elif choice == "2":
         if input("groups-Tabelle wird überschrieben. Sicher (J/n)? ") == "J":
            print("* zurücksetzen der groups-Tabelle")
            # remove all old class definitions
            sql = "DELETE FROM groups"
            cursor.execute(sql)
            print("  - alte Einträge gelöscht")
            
            # iterate through dict and for each group create classes with colors of one colorgroup
            cohort_counter = 0
            group_counter = 0
            for cohort in cohorts:
               for group in cohorts[cohort]:
                  sql = "INSERT INTO groups (name, cohort, color) VALUES (%s, %s, %s)"
                  val = (group, cohort, colorgroups[cohort_counter][group_counter])
                  cursor.execute(sql, val)
                  print("  - Gruppe ", group, " aus Kohorte ", cohort, " mit Farbe ", colorgroups[cohort_counter][group_counter], " angelegt")
                  group_counter += 1
               cohort_counter += 1
               group_counter = 0         
            print("---> erledigt (Achtung - die Events-Tabelle wird nach dem Schreiben vermutlich nicht mehr funktionieren!)")
         else:
            print("X Abbruch")
         pass
      elif choice == "3":
         if input("Sicher (J/n)? ") == "J":
            print("* zurücksetzen der events-Tabelle")
            # remove all old subjects
            sql = "DELETE  FROM events"
            cursor.execute(sql)
            print("  - alte Einträge gelöscht")
            # insert example-data to database ??
            
            print("---> erledigt")
         else:
            print("X Abbruch")
         pass
      elif choice == "4":
         if input("Farben werden überschrieben. Sicher (J/n)? ") == "J":
            print("* Farben werden überschrieben (Klassen müssen vorhanden sein!)")
            # iterate through dict and for each group read classes and update color according to colorgroup
            cohort_counter = 0
            group_counter = 0
            for cohort in cohorts:
               for group in cohorts[cohort]:
                  sql = "UPDATE groups SET color = '" + colorgroups[cohort_counter][group_counter] + "' WHERE name = '" + group + "'"
                  cursor.execute(sql)
                  print("  - Gruppe ", group, " aus Kohorte ", cohort, " hat jetzt Farbe ", colorgroups[cohort_counter][group_counter])
                  group_counter += 1
               cohort_counter += 1
               group_counter = 0         
            print("---> erledigt")
         else:
            print("X Abbruch")
         pass

      elif choice == "S!":
         db.commit()
         print("+ DATEN GESCHRIEBEN")
         break
      elif choice == "x":
         db.rollback()
         print("+ DATEN VERWORFEN")
         break
   cursor.close()
   print("- PROGRAMMENDE -")
